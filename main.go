package main

import (
	"context"
	"crypto/tls"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
    "net/http"
	"os"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"github.com/go-sql-driver/mysql"
)

func main() {
	var (
		dbUser          string
		dbPassword      string
		dbHost          string
		dbName          string
		esURL           string
		esUsername      string
		esPassword      string
		esSkipTLSVerify bool
		debugMode       bool
		logDirectory    string
		indexNamePrefix string
	)

	flag.StringVar(&dbUser, "db-user", "ostusr", "MySQL username (takes precedence over DB_USER environment variable)")
	flag.StringVar(&dbPassword, "db-password", "password", "MySQL password (takes precedence over DB_PASSWORD environment variable)")
	flag.StringVar(&dbHost, "db-host", "localhost", "MySQL host (takes precedence over DB_HOST environment variable)")
	flag.StringVar(&dbName, "db-name", "ost", "MySQL database name (takes precedence over DB_NAME environment variable)")
	flag.StringVar(&esURL, "es-url", "", "Elasticsearch URL (takes precedence over ES_URL environment variable)")
	flag.BoolVar(&debugMode, "debug", false, "Enable debug mode (takes precedence over DEBUG environment variable)")
	flag.StringVar(&logDirectory, "log-directory", "/var/log", "Log directory (default: /var/log)")
	flag.StringVar(&indexNamePrefix, "index-name-prefix", "osticket", "Prefix for the OpenSearch index name (default: osticket)")
	flag.StringVar(&esUsername, "es-username", "elasticsearch", "OpenSearch username (takes precedence over ES_USERNAME environment variable)")
	flag.StringVar(&esPassword, "es-password", "password", "OpenSearch password (takes precedence over ES_PASSWORD environment variable)")
    flag.BoolVar(&esSkipTLSVerify, "es-skip-tls-verify", false, "Skip TLS certificate verification for OpenSearch (insecure)")


	flag.Parse()

	if dbUserEnv := os.Getenv("DB_USER"); dbUserEnv != "" {
		dbUser = dbUserEnv
	}
	if dbPasswordEnv := os.Getenv("DB_PASSWORD"); dbPasswordEnv != "" {
		dbPassword = dbPasswordEnv
	}
	if dbHostEnv := os.Getenv("DB_HOST"); dbHostEnv != "" {
		dbHost = dbHostEnv
	}
	if dbNameEnv := os.Getenv("DB_NAME"); dbNameEnv != "" {
		dbName = dbNameEnv
	}
	if esUsernameEnv := os.Getenv("ES_USERNAME"); esUsernameEnv != "" {
		esUsername = esUsernameEnv
	}
	if esPasswordEnv := os.Getenv("ES_PASSWORD"); esPasswordEnv != "" {
		esPassword = esPasswordEnv
	}

	// If Elasticsearch URL is provided, initialize Elasticsearch client
	var esClient *elasticsearch.Client
	if esURL != "" {
		esCfg := elasticsearch.Config{
			Addresses: []string{esURL},
			Username:  esUsername,
			Password:  esPassword,
		}
		if esSkipTLSVerify {
            esCfg.Transport = &http.Transport{
                TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
            }
        }
		var err error
		esClient, err = elasticsearch.NewClient(esCfg)
		if err != nil {
			log.Fatal(err)
		}
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s", dbUser, dbPassword, dbHost, dbName)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	lastLogID := getLastLogID(db) - 1
	pollingInterval := 30 * time.Second

	for {
		if debugMode {
			fmt.Println("lastLogID:", lastLogID)
		}

		rows, err := db.Query("SELECT log_id, log_type, title, log, created, ip_address, updated FROM ost_syslog WHERE log_id > ?", lastLogID)
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		for rows.Next() {
			var (
				logID     int
				logType   string
				title     string
				logData   string
				created   mysql.NullTime
				ipAddress string
				updated   mysql.NullTime
			)
			err := rows.Scan(&logID, &logType, &title, &logData, &created, &ipAddress, &updated)
			if err != nil {
				log.Fatal(err)
			}

			logDoc := map[string]interface{}{
				"log_id":     logID,
				"log_type":   logType,
				"title":      title,
				"log":        logData,
				"created":    created.Time,
				"ip_address": ipAddress,
				"updated":    updated.Time,
			}

			logJSON, err := json.Marshal(logDoc)
			if err != nil {
				log.Fatal(err)
			}

			if debugMode {
				fmt.Println(string(logJSON))
			}

			currentDate := time.Now().Format("2006.01.02")
			indexName := fmt.Sprintf("%s-%s", indexNamePrefix, currentDate)

			if esClient != nil {
				req := esapi.IndexRequest{
					Index:      indexName,
					DocumentID: fmt.Sprintf("%d", logID),
					Body:       strings.NewReader(string(logJSON)),
				}

				res, err := req.Do(context.Background(), esClient)
				if err != nil {
					log.Fatal(err)
				}
				defer res.Body.Close()
				if res.IsError() {
					log.Fatalf("Error indexing document: %s", res.String())
				}

				fmt.Printf("Log %d sent to Elasticsearch\n", logID)
			} else {
				// Log to file in a coind of syslog format
				logToFile(logDirectory, created.Time, logType, logID, title, logData, ipAddress)
			}

			lastLogID = logID
		}

		time.Sleep(pollingInterval)
	}
}

func getLastLogID(db *sql.DB) int {
	var lastLogID int
	err := db.QueryRow("SELECT MAX(log_id) FROM ost_syslog").Scan(&lastLogID)
	if err != nil {
		log.Fatal(err)
	}
	return lastLogID
}

func logToFile(logDirectory string, created time.Time, logType string, logID int, title string, logData string, ipAddress string) {

	// Construct the log file path and open the file
	fileName := fmt.Sprintf("%s/%s.log", logDirectory, "osticket")
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal("Error opening log file: %v", err)
		return
	}
	defer file.Close()

	// Format log entry with additional information
	logEntry := fmt.Sprintf("%s %s: log_id=%d log_type=%s title=%s log_data=%s ip_address=%s\n",
		created.Format(time.RFC3339), "osticket", logID, logType, title, logData, ipAddress)

	// Write the log entry to the file
	if _, err := file.WriteString(logEntry); err != nil {
		log.Printf("Error writing to log file: %v", err)
	}
}
