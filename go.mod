module osticket-syslog.go

go 1.19

require (
	github.com/elastic/go-elasticsearch/v7 v7.13.1
	github.com/go-sql-driver/mysql v1.7.1
)

require github.com/elastic/elastic-transport-go/v8 v8.0.0-20230329154755-1a3c63de0db6 // indirect
